//
//  SettingViewController.m
//  slipperySlickProductions
//
//  Created by Mrinal Khullar on 7/13/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "SettingViewController.h"

@interface SettingViewController ()


@end

@implementation SettingViewController
@synthesize outletOfNotificationBtn;


- (void)viewDidLoad
{
 
    UISwitch *mySwitch = [[UISwitch alloc] initWithFrame:CGRectMake(self.view.frame.size.width-70.0,outletOfNotificationBtn.frame.origin.y, 20.0,10.0)];
   // mySwitch.backgroundColor = [UIColor whiteColor];
    mySwitch.layer.cornerRadius = 16.0;
    [self.view addSubview:mySwitch];

    
    [super viewDidLoad];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)notificationsBtn:(id)sender
{
    NSLog(@"notificationsBtn clicked");
}


- (IBAction)signOutBtn:(id)sender
{
    NSLog(@"signOutBtn clicked");
    
   
    welcomeView * viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"goBack"];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:viewController];
    [self presentViewController:nav animated:NO completion:nil];

    
     }
@end
