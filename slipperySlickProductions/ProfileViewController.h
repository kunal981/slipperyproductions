//
//  ProfileViewController.h
//  slipperySlickProductions
//
//  Created by Mrinal Khullar on 7/13/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectedSongController.h"


@interface ProfileViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong) NSArray *postImg;
@property(nonatomic,strong) NSArray *postSong;
@property(nonatomic,strong) NSArray *postTime;
@property(nonatomic,strong) NSArray *postArtist;
@property(nonatomic,strong) NSArray *postViews;

@property(nonatomic,strong) NSArray *playlistImg;
@property(nonatomic,strong) NSArray *playlistSong;
@property(nonatomic,strong) NSArray *playlistTime;
@property(nonatomic,strong) NSArray *playlistArtist;
@property(nonatomic,strong) NSArray *playlistViews;

@property (weak, nonatomic) IBOutlet UIView *profileImageView;
@property (weak, nonatomic) IBOutlet UIView *profileSubviewObj;

@property (weak, nonatomic) IBOutlet UITableView *postsTableBiew;
@property (weak, nonatomic) IBOutlet UITableView *playlistTableView;
@property (weak, nonatomic) IBOutlet UIView *highlightView;

- (IBAction)infoBtn:(UIButton *)sender;

- (IBAction)postsBtn:(UIButton *)sender;

- (IBAction)playlistsBtn:(UIButton *)sender;

@end
