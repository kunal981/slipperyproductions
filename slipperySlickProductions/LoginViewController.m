//
//  ViewController.m
//  slipperySlickProductions
//
//  Created by Mrinal Khullar on 7/9/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "LoginViewController.h"
#import "ProfileViewController.h"
#import "AlbumViewController.h"
#import "SettingViewController.h"
#import "DiscoverViewController.h"



@interface LoginViewController ()
{
    MBProgressHUD *HUD;
  
}
@end

@implementation LoginViewController



@synthesize userNameTxtField,passwordTxtField,btnLogin,btnFb;
@synthesize txtFeildPassword,txtFieldUserName,outletOfBackBtn;


- (void)viewDidLoad
{
    
    
    self.navigationController.navigationBar.hidden = YES;
   
    
    txtFeildPassword.delegate = self;
    txtFieldUserName.delegate = self;
    
    outletOfBackBtn.layer.borderWidth = 1.5f;
    outletOfBackBtn.layer.cornerRadius = 5.0f;
    outletOfBackBtn.layer.borderColor=[[UIColor whiteColor] CGColor];
    
    UIView *line1 = [UIView new];
    line1.frame = CGRectMake((self.view.frame.size.width/2)-((286.0/375.0)*self.view.frame.size.width/2), (330.0/667.0)*self.view.frame.size.height,
                             (286.0/375.0)*self.view.frame.size.width, 1.0);
    [line1 setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:line1];
    NSLog(@"width is %f",self.view.frame.size.width);
    
    UIView *line2 = [UIView new];
    line2.frame = CGRectMake((self.view.frame.size.width/2)-((286.0/375.0)*self.view.frame.size.width/2), (414.0/667.0)*self.view.frame.size.height, (286.0/375.0)*self.view.frame.size.width, 1.0);
    [line2 setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:line2];
     NSLog(@"height is %f",self.view.frame.size.height);
    
      [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithRed:28.0/255 green:28.0/255 blue:28.0/255 alpha:1.0]];

    NSAttributedString * placeholderUsername = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:17.0]}];
    userNameTxtField.attributedPlaceholder = placeholderUsername;
    
    NSAttributedString * placeholderPassword = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:17.0]}];
    passwordTxtField.attributedPlaceholder = placeholderPassword;
    
    btnLogin.layer.borderWidth = 1.5;
    btnLogin.layer.cornerRadius = 6;
    
    btnFb.layer.borderWidth = 1.5;
    btnFb.layer.cornerRadius = 6;
    

  }


#pragma mark - Execution code

- (void)myTask
{
    
    sleep(1.5);
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txtFeildPassword resignFirstResponder ];
    [txtFieldUserName resignFirstResponder ];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
     [txtFeildPassword resignFirstResponder];
     [txtFieldUserName resignFirstResponder];
    
    return YES;
    
}

-(void)login
{
    
    NSString *post = [NSString stringWithFormat:@"username=%@&password=%@",userNameTxtField.text ,passwordTxtField.text];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:loginAPI]];
    
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSURLConnection *theConnection = [NSURLConnection connectionWithRequest:request delegate:self];
    
    if( theConnection )
    {
        HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        [self.navigationController.view addSubview:HUD];
        HUD.delegate = self;
        HUD.labelText = @"Loading";
        HUD.detailsLabelText = @"updating data";
        HUD.dimBackground = YES;
        [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];


        
    }
   
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"response is %@", response);
    
    loginMutableData = [[NSMutableData alloc]init];
    
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
    loginMutableData = [[NSMutableData alloc]initWithData:data];
    
    
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    id result = [NSJSONSerialization JSONObjectWithData:loginMutableData options:kNilOptions error:nil];
    NSLog(@"result is %@",result);
    
    BOOL success = [[result objectForKey:@"success"] boolValue];
   
    
    if (!success)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Invalid username and password" delegate:self cancelButtonTitle:@"Ok"otherButtonTitles: nil];
        [alert show];
        
        
    }
    else
    {
        NSLog(@"login in else part");
       [HUD hide:YES];
        [self tabBar];
    }
    
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@",error);
    
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:error delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//    [alert show];
    
}


-(void)tabBar
{
    
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.delegate = self;
    [self.tabBarController.tabBar setTranslucent:NO];
    self.tabBarController.tabBar.barTintColor = [UIColor colorWithRed:0.0000 green:0.7020 blue:0.8980 alpha:1.0];
    self.tabBarController.tabBar.tintColor = [UIColor whiteColor];
    
    
    
    NSMutableArray *viewControllers = [[NSMutableArray alloc] init];
    
    // first tab has view controller in navigation controller
    ProfileViewController* restaurantTable= [self.storyboard instantiateViewControllerWithIdentifier:@"profile"];
    restaurantTable.tabBarItem.image=[[UIImage imageNamed:@"profileUnselected.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    restaurantTable.tabBarItem.title=@"Profile";
    [restaurantTable.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    restaurantTable.tabBarItem.selectedImage=[UIImage imageNamed:@"profileSelected.png"];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:restaurantTable];
    [viewControllers addObject:navController];
    
    AlbumViewController* nearMeView = [self.storyboard instantiateViewControllerWithIdentifier:@"album"];
    nearMeView.tabBarItem.image=[[UIImage imageNamed:@"albumUnselected.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    nearMeView.tabBarItem.title=@"Album";
    
    [nearMeView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    nearMeView.tabBarItem.selectedImage=[UIImage imageNamed:@"albumSelected.png"];
    UINavigationController *navToNearMe = [[UINavigationController alloc] initWithRootViewController:nearMeView];
    [viewControllers addObject:navToNearMe];
    
    DiscoverViewController* searchView = [self.storyboard instantiateViewControllerWithIdentifier:@"discover"];
    searchView.tabBarItem.image=[[UIImage imageNamed:@"discover.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    searchView.tabBarItem.title=@"Discover";
    [searchView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    searchView.tabBarItem.selectedImage=[UIImage imageNamed:@"discoverSelected.png"];
    UINavigationController *navTosearchView = [[UINavigationController alloc] initWithRootViewController:searchView];
    [viewControllers addObject:navTosearchView];
    
    SettingViewController* settingView=[self.storyboard instantiateViewControllerWithIdentifier:@"setting"];
    settingView.tabBarItem.image=[[UIImage imageNamed:@"settingUnselected.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    settingView.tabBarItem.title=@"Settings";
    [settingView.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica-Bold" size:12],NSFontAttributeName,nil]forState:UIControlStateNormal];
    settingView.tabBarItem.selectedImage=[UIImage imageNamed:@"settingSelected.png"];
    
    UINavigationController *navTosettingView = [[UINavigationController alloc] initWithRootViewController:settingView];
    [viewControllers addObject:navTosettingView];
    
     [self.tabBarController setViewControllers:viewControllers];
    
    // add tabbar and show
    [[self view] addSubview:[self.tabBarController view]];
    
    
   [self.navigationController pushViewController:self.tabBarController animated:YES];
    
   // [self presentViewController:self.tabBarController animated:NO completion:nil];
    
}
//- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    if ([textField isEqual:txtFieldUserName])
//    {
//        if ([string isEqualToString:@" "] )
//        {
//        
//            string = [txtFieldUserName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//            NSLog(@" trimmed username is %@ ",string);
//        
//            return NO;
//        }
//        
//    }
//    if ([textField isEqual:txtFeildPassword])
//    {
//        if ([string isEqualToString:@" "] )
//        {
//            string = [txtFeildPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//            NSLog(@"trimmed password is %@",string);
//            
//            return NO;
//            
//
//        }
//    }
//    
//    return YES;
//}
//
- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField isEqual:txtFieldUserName])
    {
        if ([string isEqualToString:@" "] )
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Username can't be filled with blank space" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];

            return NO;
        
        }
    }
    if ([textField isEqual:txtFeildPassword])
    {
        if ([string isEqualToString:@" "] )
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Password can't be filled with blank space" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            
            return NO;
            
        }
    }

    return YES;
}

- (IBAction)btnLogin:(id)sender
{
    
    if([txtFieldUserName.text isEqualToString:@""] && [txtFeildPassword.text isEqualToString:@""] )
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"All text fields are mandatory" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
        
    }
    
     else if ([txtFieldUserName.text isEqualToString:@""])
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Fill your username" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        

    }
    else if ([txtFeildPassword.text isEqualToString:@""])
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Fill your password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
        
    }
   
    else
    {
      
     [self login];
        
    }
       
   }

- (IBAction)btnFb:(id)sender
{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *errorObj)
    {
        if (errorObj)
        {
            // Process error
            NSLog(@"error %@",errorObj);
        }
        
        else if (result.isCancelled)
        {
            // Handle cancellations
            NSLog(@"Cancelled");
        }
        
        else
            
        {
            if ([result.grantedPermissions containsObject:@"email"])
            {
                
                NSLog(@"%@",result);
                NSLog(@"Correct");
                HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                [self.navigationController.view addSubview:HUD];
                HUD.delegate = self;
                HUD.labelText = @"Loading";
                HUD.detailsLabelText = @"updating data";
                HUD.dimBackground = YES;
                [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
                
                
                if ([FBSDKAccessToken currentAccessToken])
                {
                    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
                     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,id result, NSError *error)
                    {
                        
                        if (!error)
                         {
                              [self tabBar];
                             NSLog(@"fetched user:%@", result);
                         }
                     }];
                }
               
            }
        }
    }];
    
   }

- (IBAction)btnForgotPassword:(id)sender
{
    ForgotPasswordViewController *forgot = [self.storyboard instantiateViewControllerWithIdentifier:@"pushgo"];
    [self presentViewController:forgot animated:NO completion:nil ];
}

- (IBAction)backBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}




@end
