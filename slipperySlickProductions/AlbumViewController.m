//
//  AlbumViewController.m
//  slipperySlickProductions
//
//  Created by Mrinal Khullar on 7/10/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "AlbumViewController.h"
#include "SettingViewController.h"

@interface UIViewController ()

@end

@implementation AlbumViewController
@synthesize imgArray,categoryArray,trackArray;


- (void)viewDidLoad
{
    
       
    _txtFieldOfSearchBar.delegate = self;

    [[_outletOfUploadView layer] setBorderWidth:1.5f];
    _outletOfUploadView.layer.cornerRadius = 3.5f;
    [[_outletOfUploadView layer] setBorderColor:[UIColor whiteColor].CGColor];
    
       [super viewDidLoad];
    
    
    
    categoryArray = [NSArray arrayWithObjects:@"Popular Audio",@"Top Beats",@"Dance",@"Disco",@"Drum and Bass",@"Folk",@"Hip-Hop",@"Jazz", nil ];
    trackArray = [NSArray arrayWithObjects:@"62 Tracks",@"43 Tracks",@"45 Tracks",@"92 Tracks",@"16 Tracks",@"22 Tracks",@"60 Tracks",@"34 Tracks", nil];
    imgArray = [NSArray arrayWithObjects: @"firstImage640*250.png",@"thirdImage640*250.jpg",@"secImage640*250.jpg",@"fourthImage640x250.jpg",@"artist5.jpeg",@"artist6.jpeg",@"artist7.jpeg",@"artist11.jpeg",nil];
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    MBProgressHUD *HUD;
    
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.delegate = self;
    HUD.labelText = @"Loading";
    HUD.detailsLabelText = @"updating data";
    HUD.dimBackground = YES;
    [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
}

#pragma mark - Execution code

- (void)myTask
{
    
    sleep(1.5);
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_txtFieldOfSearchBar resignFirstResponder];
   
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
   [_txtFieldOfSearchBar resignFirstResponder];
    
   return YES;
    
}



-(IBAction)pressed:(id)sender
{
    NSLog(@"UPLOAD button pressed");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [imgArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    

    cell.layoutMargins = UIEdgeInsetsZero;
    cell.preservesSuperviewLayoutMargins = false;
    tableView.separatorInset = UIEdgeInsetsZero;
    
    
    
    UILabel *albumCat = [UILabel new];
   // albumCat.frame = CGRectMake(100, 12, 200, 25);
    albumCat.frame = CGRectMake((70.0/375)*self.view.frame.size.width,(12.0/667.0)*self.view.frame.size.height, (150.0/375.0)*self.view.frame.size.width, 25.0);
    [albumCat setTextColor:[UIColor blackColor]];
    [albumCat setBackgroundColor:[UIColor whiteColor]];
    albumCat.font = [UIFont systemFontOfSize:14.0];
    [cell.contentView addSubview: albumCat] ;
    albumCat.text = categoryArray[indexPath.row];
    
    
    UILabel *lblTrack = [UILabel new];
  //  lblTrack.frame = CGRectMake(250, 12, 150, 25);
   lblTrack.frame = CGRectMake((250.0/375)*self.view.frame.size.width, (12.0/667.0)*self.view.frame.size.height, (150.0/375.0)*self.view.frame.size.width, 25.0);
    [lblTrack setTextColor:[UIColor blackColor]];
    [lblTrack setBackgroundColor:[UIColor whiteColor]];
     lblTrack.font = [UIFont systemFontOfSize:14.0];
    [cell.contentView addSubview: lblTrack];
    lblTrack.text = trackArray[indexPath.row];
    
    
    UIImageView *lblImg = [[UIImageView alloc]init];
    lblImg.frame = CGRectMake(2, 5, 40, 40);
    lblImg.image = [UIImage imageNamed:[imgArray objectAtIndex:indexPath.row]];
    [cell.contentView addSubview:lblImg];
    
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SelectedCategoryController *select = [self.storyboard instantiateViewControllerWithIdentifier:@"selectMove"];
    [self.navigationController pushViewController:select animated:NO];
    
    
    
}
-(BOOL)trimAudiofile
{
    
    float audioStartTime;//define start time of audio
    float audioEndTime;//define end time of audio
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd_HH-mm-ss"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *libraryCachesDirectory = [paths objectAtIndex:0];
    libraryCachesDirectory = [libraryCachesDirectory stringByAppendingPathComponent:@"Caches"];
    NSString *OutputFilePath = [libraryCachesDirectory stringByAppendingFormat:@"taylor swift-you belong with me%@.mp3", [dateFormatter stringFromDate:[NSDate date]]];
    NSURL *audioFileOutput = [NSURL fileURLWithPath:OutputFilePath];
    NSURL *audioFileInput;//<Path of orignal audio file>
    
    if (!audioFileInput || !audioFileOutput)
    {
        return NO;
    }
    
    [[NSFileManager defaultManager] removeItemAtURL:audioFileOutput error:NULL];
    AVAsset *asset = [AVAsset assetWithURL:audioFileInput];
    
    AVAssetExportSession *exportSession = [AVAssetExportSession exportSessionWithAsset:asset presetName:AVAssetExportPresetAppleM4A];
    if (exportSession == nil)
    {
        return NO;
    }
    
    CMTime startTime = CMTimeMake((int)(floor(audioStartTime * 100)), 100);
    CMTime stopTime = CMTimeMake((int)(ceil(audioEndTime * 100)), 100);
    CMTimeRange exportTimeRange = CMTimeRangeFromTimeToTime(startTime, stopTime);
    
    exportSession.outputURL = audioFileOutput;
    exportSession.timeRange = exportTimeRange;
    exportSession.outputFileType = AVFileTypeAppleM4A;
    
    [exportSession exportAsynchronouslyWithCompletionHandler:^
    
     {
         if (AVAssetExportSessionStatusCompleted == exportSession.status)
         {
             NSLog(@"Export OK");
         }
         else if (AVAssetExportSessionStatusFailed == exportSession.status)
         {
             NSLog(@"Export failed: %@", [[exportSession error] localizedDescription]);
         }
     }];
    
    return YES;
}

- (IBAction)uploadBtn:(id)sender
{
    
        NSLog(@"upload btn clicked");
    
  
}
@end
