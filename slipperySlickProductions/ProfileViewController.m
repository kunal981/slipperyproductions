//
//  ProfileViewController.m
//  slipperySlickProductions
//
//  Created by Mrinal Khullar on 7/13/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()
{
    CGFloat sizeHeight;
}
@end

@implementation ProfileViewController
@synthesize postArtist,postImg,postSong,postTime,postViews,postsTableBiew,highlightView;
@synthesize playlistArtist,playlistImg,playlistSong,playlistTime,playlistViews,playlistTableView;
@synthesize profileSubviewObj;
- (void)viewDidLoad
{
           self.navigationItem.hidesBackButton = YES;
    [self.navigationController.navigationBar setTranslucent:NO];
    [profileSubviewObj.layer setBorderWidth:1.0f];
   profileSubviewObj.layer.borderColor = [UIColor grayColor].CGColor;
    
    
    sizeHeight = [UIScreen mainScreen].bounds.size.height;
   
              [super viewDidLoad];
    
    postImg = [NSArray arrayWithObjects:@"artist11.jpeg",@"firstImage640*250.png",@"secImage640*250.jpg", nil];
    postArtist = [NSArray arrayWithObjects:@"Selena Gomez",@"Lucy Hale",@"Miley Cyrus", nil];
    postSong = [NSArray arrayWithObjects:@"Made with the Flaxen Hair.mp3",@"Footloose:Blue-Ray",@"Kenny Loggins Talks Footloose", nil];
    postViews = [NSArray arrayWithObjects:@"7,123",@"1,456",@"12,009", nil];
    postTime = [NSArray arrayWithObjects:@"3:33",@"3:26",@"2:33", nil];
    
    playlistImg = [NSArray arrayWithObjects: @"artist10.jpg",@"fourthImage640x250.jpg",@"artist11.jpeg", nil];
    playlistArtist = [NSArray arrayWithObjects:@"Miley Cyrus",@"Jordan Fisher ",@"Selena Gomez", nil];
    playlistSong = [NSArray arrayWithObjects:@"Footloose:Blue-Ray",@"Kenny Loggins Talks Footloose",@"Made with the Flaxen Hair.mp3", nil];
    playlistViews = [NSArray arrayWithObjects:@"1,456",@"7,123",@"1,098", nil];
    playlistTime = [NSArray arrayWithObjects:@"2:33",@"3:26",@"3:33", nil];

  [postsTableBiew reloadData];
  [playlistTableView reloadData];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == postsTableBiew)
    {
        return 1;
    }
    
    else
    {
        return 1;
    }
   
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == postsTableBiew)
    {
        return [postImg count];
    }
    
    else
    {
        return [playlistImg count];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == postsTableBiew)
    {
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"postIdentifier"];
    if (cell == nil)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"postIdentifier" forIndexPath:indexPath];
        NSLog(@"cell found");
    }
    
    cell.layoutMargins = UIEdgeInsetsZero;
    cell.preservesSuperviewLayoutMargins = false;
    tableView.separatorInset = UIEdgeInsetsZero;
    
    UILabel *albumTrack = [UILabel new];
    albumTrack.frame = CGRectMake(self.view.frame.size.width-35, 0, 50, 20);
    [albumTrack setTextColor:[UIColor lightGrayColor]];
    albumTrack.font = [UIFont systemFontOfSize:12.0];
    [albumTrack setBackgroundColor:[UIColor whiteColor]];
    [cell.contentView addSubview: albumTrack];
    albumTrack.text = postTime[indexPath.row];
    
    
    
    UIImageView *lblImg = [[UIImageView alloc]init];
    lblImg.frame = CGRectMake(2, 5, 60, 60);
    lblImg.image = [UIImage imageNamed:postImg[indexPath.row]];
    [cell.contentView addSubview:lblImg];
    
    UILabel *albumArtist = [UILabel new];
    albumArtist.frame = CGRectMake(68, 0, 180, 25);
    [albumArtist setTextColor:[UIColor lightGrayColor]];
    albumArtist.font = [UIFont systemFontOfSize:12.0];
    [albumArtist setBackgroundColor:[UIColor whiteColor]];
    [cell.contentView addSubview: albumArtist] ;
    albumArtist.text = postArtist[indexPath.row];
    
    UILabel *albumCat = [UILabel new];
    albumCat.frame = CGRectMake(68, 23, 240, 18);
    [albumCat setTextColor:[UIColor blackColor]];
    albumCat.font = [UIFont systemFontOfSize:15.0];
    [albumCat setBackgroundColor:[UIColor whiteColor]];
    [cell.contentView addSubview: albumCat] ;
    albumCat.text = postSong[indexPath.row];
    
    
    UIImageView *staticImg = [[UIImageView alloc]init];
    staticImg.frame = CGRectMake(lblImg.frame.size.width+10,lblImg.frame.size.height-9, 13, 13);
    staticImg.image = [UIImage imageNamed:@"playStaticImage.png"];
    [cell.contentView addSubview:staticImg];
    
    UILabel *albumViews = [UILabel new];
    albumViews.frame = CGRectMake(lblImg.frame.size.width+28, 50, 40, 15);
    [albumViews setTextColor:[UIColor lightGrayColor]];
    albumViews.font = [UIFont systemFontOfSize:12.0];
    [albumViews setBackgroundColor:[UIColor whiteColor]];
    [cell.contentView addSubview: albumViews];
    albumViews.text = postViews[indexPath.row];
    
    return cell;
    
}
    else
    {
        UITableViewCell *playlistCell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"playlistIdentifier"];
        if (playlistCell == nil)
        {
            playlistCell = [tableView dequeueReusableCellWithIdentifier:@"playlistIdentifier" forIndexPath:indexPath];
            NSLog(@"cell found");
        }
        
        playlistCell.layoutMargins = UIEdgeInsetsZero;
        playlistCell.preservesSuperviewLayoutMargins = false;
        tableView.separatorInset = UIEdgeInsetsZero;
        
        UILabel *albumTrack = [UILabel new];
        albumTrack.frame = CGRectMake(self.view.frame.size.width-35, 0, 50, 20);
        [albumTrack setTextColor:[UIColor lightGrayColor]];
        albumTrack.font = [UIFont systemFontOfSize:12.0];
        [albumTrack setBackgroundColor:[UIColor whiteColor]];
        [playlistCell.contentView addSubview: albumTrack];
        albumTrack.text = playlistTime[indexPath.row];
        
        
        
        UIImageView *lblImg = [[UIImageView alloc]init];
        lblImg.frame = CGRectMake(2, 5, 60, 60);
        lblImg.image = [UIImage imageNamed:playlistImg[indexPath.row]];
        [playlistCell.contentView addSubview:lblImg];
        
        UILabel *albumArtist = [UILabel new];
        albumArtist.frame = CGRectMake(68, 0, 180, 25);
        [albumArtist setTextColor:[UIColor lightGrayColor]];
        albumArtist.font = [UIFont systemFontOfSize:12.0];
        [albumArtist setBackgroundColor:[UIColor whiteColor]];
        [playlistCell.contentView addSubview: albumArtist] ;
        albumArtist.text = playlistArtist[indexPath.row];
        
        UILabel *albumCat = [UILabel new];
        albumCat.frame = CGRectMake(68, 23, 240, 18);
        [albumCat setTextColor:[UIColor blackColor]];
        albumCat.font = [UIFont systemFontOfSize:15.0];
        [albumCat setBackgroundColor:[UIColor whiteColor]];
        [playlistCell.contentView addSubview: albumCat] ;
        albumCat.text = playlistSong[indexPath.row];
        
        
        UIImageView *staticImg = [[UIImageView alloc]init];
        staticImg.frame = CGRectMake(lblImg.frame.size.width+10,lblImg.frame.size.height-9, 13, 13);
        staticImg.image = [UIImage imageNamed:@"playStaticImage.png"];
        [playlistCell.contentView addSubview:staticImg];
        
        UILabel *albumViews = [UILabel new];
        albumViews.frame = CGRectMake(lblImg.frame.size.width+28, 50, 40, 15);
        [albumViews setTextColor:[UIColor lightGrayColor]];
        albumViews.font = [UIFont systemFontOfSize:12.0];
        [albumViews setBackgroundColor:[UIColor whiteColor]];
        [playlistCell.contentView addSubview: albumViews];
        albumViews.text = playlistViews[indexPath.row];
        
        return playlistCell;

    }


}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SelectedSongController *select = [self.storyboard instantiateViewControllerWithIdentifier:@"selectedSong"];
    
    if(tableView == postsTableBiew)
    {
        
        select.strImage = postImg[indexPath.row];
        select.strNavigationTitle = postSong[indexPath.row];
        select.strSongTotalTime = postTime[indexPath.row];
        select.strArtistName = postArtist[indexPath.row];
    }
    else
        
    {
        
        select.strImage = playlistImg[indexPath.row];
        select.strNavigationTitle = playlistSong[indexPath.row];
        select.strSongTotalTime = playlistTime[indexPath.row];
        select.strArtistName = playlistArtist[indexPath.row];
    }
    [self.navigationController pushViewController:select animated:NO];
    
}

- (IBAction)postsBtn:(UIButton *)sender
{
    postsTableBiew.hidden = NO;
    playlistTableView.hidden = YES;
    if (sizeHeight == 480)
    {
        highlightView.frame = CGRectMake(sender.frame.origin.x,profileSubviewObj.frame.size.height-6, sender.frame.size.width, highlightView.frame.size.height);

    }
    else if(sizeHeight == 568)
    {
    highlightView.frame = CGRectMake(sender.frame.origin.x,profileSubviewObj.frame.size.height-7, sender.frame.size.width, highlightView.frame.size.height);
    }
    else
    {
         highlightView.frame = CGRectMake(sender.frame.origin.x,profileSubviewObj.frame.size.height-9, sender.frame.size.width, highlightView.frame.size.height);
    }
}

- (IBAction)playlistsBtn:(UIButton *)sender
{
    postsTableBiew.hidden = YES;
    playlistTableView.hidden = NO;
    if (sizeHeight == 480)
    {
        highlightView.frame = CGRectMake(sender.frame.origin.x,profileSubviewObj.frame.size.height-6, sender.frame.size.width, highlightView.frame.size.height);
        
    }
    else if(sizeHeight == 568)
    {
    highlightView.frame = CGRectMake(sender.frame.origin.x,profileSubviewObj.frame.size.height-7, sender.frame.size.width, highlightView.frame.size.height);
    }
    else
    {
        highlightView.frame = CGRectMake(sender.frame.origin.x,profileSubviewObj.frame.size.height-9, sender.frame.size.width, highlightView.frame.size.height);
    }
}

- (IBAction)infoBtn:(UIButton *)sender
{
    postsTableBiew.hidden = YES;
    playlistTableView.hidden = YES;
    if (sizeHeight == 480)
    {
        highlightView.frame = CGRectMake(sender.frame.origin.x,profileSubviewObj.frame.size.height-6, sender.frame.size.width, highlightView.frame.size.height);
        
    }

     else if(sizeHeight == 568)
     {
        highlightView.frame = CGRectMake(sender.frame.origin.x,profileSubviewObj.frame.size.height-7, sender.frame.size.width, highlightView.frame.size.height);
    }
     else
     {
         highlightView.frame = CGRectMake(sender.frame.origin.x,profileSubviewObj.frame.size.height-9, sender.frame.size.width, highlightView.frame.size.height);
     }
}
@end
