//
//  SelectedSongController.m
//  slipperySlickProductions
//
//  Created by Mrinal Khullar on 7/13/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "SelectedSongController.h"

@interface SelectedSongController ()



@end

@implementation SelectedSongController


@synthesize strImage,imageViewOutlet,strNavigationTitle,songLbl,strSongTotalTime,songTotalTimeLbl,artistLbl,strArtistName;
@synthesize outletOfPauseBtn,outletOfPlayBtn;
@synthesize positionSlider,timeIncrease;
@synthesize theSoundArray;





- (void)viewDidLoad
{
    self.navigationItem.hidesBackButton = YES;
    
    
   filePath = [[NSBundle mainBundle] pathForResource:@"Hilary Duff - I am Super Girl" ofType:@"mp3"];
   fileURL = [[NSURL alloc] initFileURLWithPath:filePath];
    
    objAudio = [[AVAudioPlayer alloc]initWithContentsOfURL:fileURL error:nil];
   
    
    SettingViewController *stop = [SettingViewController new];
    stop.strStopAudio =  filePath;
   
    objAudio.delegate = self;
    objAudio.volume = 1.0;
    objAudio.numberOfLoops = INFINITY;
    [objAudio prepareToPlay];
    positionSlider.maximumValue = objAudio.duration;
    positionSlider.minimumValue = 0.0f;
    objAudio.currentTime = positionSlider.value;
    
    [objAudio play];
    
    
    [self updateDisplay];
    
    if (!(soundTimer = nil))
    {
        soundTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
        NSLog(@"soundTimer condition working");
        
        
    }
   
    _outletOfReverseBtn.layer.cornerRadius = _outletOfReverseBtn.frame.size.width/2;
    _outletOfFarwordBtn.layer.cornerRadius = _outletOfFarwordBtn.frame.size.width/2;
    outletOfPauseBtn.layer.cornerRadius = outletOfPauseBtn.frame.size.width/2;
    outletOfPlayBtn.layer.cornerRadius = outletOfPlayBtn.frame.size.width/2;
    
    
    UIButton *btnNext1 =[[UIButton alloc] init];
    [btnNext1 setBackgroundImage:[UIImage imageNamed:@"cross.png"] forState:UIControlStateNormal];
    
    btnNext1.frame = CGRectMake(5, 5, 20, 20);
    UIBarButtonItem *btnNext =[[UIBarButtonItem alloc] initWithCustomView:btnNext1];
    [btnNext1 addTarget:self action:@selector(backBtn:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = btnNext;
    
    [super viewDidLoad];
    
    imageViewOutlet.image = [UIImage imageNamed:strImage];
    self.navigationItem.title = strNavigationTitle;
    songLbl.text = strNavigationTitle;
    songTotalTimeLbl.text = strSongTotalTime;
    artistLbl.text = strArtistName;
    
    [self audioPlayerDidFinishPlaying:objAudio successfully:YES];
   
  }


-(void)viewWillDisappear:(BOOL)animated
{
    [objAudio stop];
}


-(void)updateSlider
{
            NSLog(@"update slider method called");
        [positionSlider setValue:objAudio.currentTime];
        [self updateDisplay];

}

-(void)playSoundInArray
{
   
}
- (void)setCurrentAudioTime:(float)value
{
    [objAudio setCurrentTime:value];
}

#pragma mark - Display Update

- (void)updateDisplay
{
    double currentTime = objAudio.currentTime;
    int minutes = floor(currentTime/60);
    int seconds = trunc(currentTime - minutes * 60);
    
    if (seconds < 10)
    {
         timeIncrease.text = [NSString stringWithFormat:@"%i:0%i" ,minutes,seconds];
    }
    else
    {
        timeIncrease.text = [NSString stringWithFormat:@"%i:%i" ,minutes,seconds];
    }
    
}

- (void)updateSliderLabels
{
    NSTimeInterval currentTime = positionSlider.value;
    NSString* currentTimeString = [NSString stringWithFormat:@"%.02f", currentTime];
    
    timeIncrease.text =  currentTimeString;
}




-(IBAction)backBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
  
}

- (IBAction)changeSliderPoistion
{
   [objAudio setCurrentTime:positionSlider.value];
    [self updateSliderLabels];
}

- (IBAction)sliderToucUpInside:(id)sender
{
  //  [objAudio stop];
    objAudio.currentTime = positionSlider.value;
    [objAudio prepareToPlay];
    [self btnPlay:self];

}



- (IBAction)btnPlay:(id)sender
{
    outletOfPlayBtn.hidden = true;
    outletOfPauseBtn.hidden = false;
    
    if (![objAudio isPlaying])
    {
        [objAudio play];
        
    }
    else
    {
        [objAudio pause];
    }
    
    
}

- (IBAction)btnPause:(id)sender
{
    outletOfPlayBtn.hidden = false;
    outletOfPauseBtn.hidden = true;
    
    if ([objAudio isPlaying])
    {
        [objAudio pause];
        [self updateDisplay];
        
    }
    
    else
    {
        [objAudio pause];
    }

}
- (IBAction)btnRepeat:(id)sender
{
    NSLog(@"Repeat button pressed");
    
//    [objAudio prepareToPlay];
    
}

- (IBAction)btnNextSong:(id)sender
{
    NSLog(@"btnNextSong Clicked ");
    
}

- (IBAction)btnPreviousSong:(id)sender
{
    NSLog(@"btnPreviousSong Clicked ");
   
}

- (IBAction)btnIncVolume:(id)sender
{
    objAudio.volume = objAudio.volume + 0.15;
  
    NSLog(@"btn volume Increase Clicked %f",objAudio.volume);
    
}

- (IBAction)btnDecVolume:(id)sender
{
    objAudio.volume = objAudio.volume - 0.15;
    
    NSLog(@"objAudio.volume for dec Volume is %f",objAudio.volume);
}

- (IBAction)shareBtnForInstagram:(id)sender
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 5.0)
    {
        float i = [[[UIDevice currentDevice] systemVersion] floatValue];
        NSString *str = [NSString stringWithFormat:@"We're sorry, but Instagram is not supported with your iOS %.1f version.", i];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:str delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    else
    {
        CGRect rect = CGRectMake(0 ,0 , 0, 0);
        
        NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Test.ig"];
        
        NSURL *igImageHookFile = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@", jpgPath]];
        NSLog(@"JPG path %@", jpgPath);
        // NSLog(@"URL Path %@", igImageHookFile);
        docFile.UTI = @"com.instagram.photo";
        // docFile = [self setupControllerWithURL:igImageHookFile usingDelegate:self];
        docFile = [UIDocumentInteractionController interactionControllerWithURL:igImageHookFile];
        [docFile presentOpenInMenuFromRect: rect    inView: self.view animated: YES ];
        
        NSURL *instagramURL = [NSURL URLWithString:@"instagram://media?id=MEDIA_ID"];
        
        if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
        {
            [[UIApplication sharedApplication] openURL:instagramURL];
            //[docFile presentOpenInMenuFromRect: rect inView: self.view animated: YES ];
        }
        
        else
        {
            NSLog(@"No Instagram Found");
        }
        
    }
    
    NSLog(@"shareBtnOnInstagram clicked ");

}

#pragma mark - Timer

- (void)timerFired:(NSTimer*)timer
{
    [soundTimer invalidate];
     soundTimer = nil;
    [self updateDisplay];
    
}

- (void)stopTimer
{
    [soundTimer invalidate];
     soundTimer = nil;
    [self updateDisplay];
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)p successfully:(BOOL)flag
{
    if (flag == YES)
    {

    
      [objAudio prepareToPlay];
    }
    
    else
    {
         NSLog(@"Playback finished unsuccessfully");
        
    }
}



@end
