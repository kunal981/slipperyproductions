//
//  SignUpViewController.h
//  slipperySlickProductions
//
//  Created by Mrinal Khullar on 7/13/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "LoginViewController.h"
#import "signUpViewController.h"

@interface welcomeView : UIViewController
@property (weak, nonatomic) IBOutlet UIView *subView;

@property (weak, nonatomic) IBOutlet UIView *viewObj;
@property(strong,nonatomic) MPMoviePlayerController *objOfmoviePlayer;

- (IBAction)LoginBtn:(id)sender;
- (IBAction)SignupBtn:(id)sender;

@end
