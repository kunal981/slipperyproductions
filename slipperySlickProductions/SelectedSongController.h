//
//  SelectedSongController.h
//  slipperySlickProductions
//
//  Created by Mrinal Khullar on 7/13/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "SettingViewController.h"

@interface SelectedSongController : UIViewController<AVAudioPlayerDelegate,UIDocumentInteractionControllerDelegate>
{
    AVAudioPlayer *objAudio;
    NSTimer *soundTimer;
    NSString *filePath;
    NSURL *fileURL;
    UIDocumentInteractionController *docFile;
    
}

@property (weak, nonatomic) IBOutlet UIImageView *imageViewOutlet;
@property (weak, nonatomic) IBOutlet UILabel *songLbl;
@property (weak, nonatomic) IBOutlet UILabel *songTotalTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *songStartingTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *artistLbl;


@property (weak, nonatomic) IBOutlet UIButton *outletOfPlayBtn;
@property (weak, nonatomic) IBOutlet UIButton *outletOfReverseBtn;
@property (weak, nonatomic) IBOutlet UIButton *outletOfPauseBtn;
@property (weak, nonatomic) IBOutlet UIButton *outletOfFarwordBtn;


@property(nonatomic,strong) NSString *strImage;
@property(nonatomic,strong) NSString *strNavigationTitle;
@property(nonatomic,strong) NSString *strSongTotalTime;
@property(nonatomic,strong) NSString *strArtistName;

- (IBAction)changeSliderPoistion;
- (IBAction)btnPlay:(id)sender;
- (IBAction)btnPause:(id)sender;
- (IBAction)btnRepeat:(id)sender;
- (IBAction)sliderToucUpInside:(id)sender;
- (IBAction)btnNextSong:(id)sender;
- (IBAction)btnPreviousSong:(id)sender;
- (IBAction)btnIncVolume:(id)sender;
- (IBAction)btnDecVolume:(id)sender;
- (IBAction)shareBtnForInstagram:(id)sender;



@property (weak, nonatomic) IBOutlet UISlider *positionSlider;
@property (weak, nonatomic) IBOutlet UILabel *timeIncrease;
@property(nonatomic,strong) NSArray *theSoundArray;


//-(void)updateSlider;

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)p successfully:(BOOL)flag;


@end


