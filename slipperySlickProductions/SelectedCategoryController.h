//
//  SelectedCategoryController.h
//  slipperySlickProductions
//
//  Created by Mrinal Khullar on 7/13/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectedSongController.h"
#import "MBProgressHUD.h"


@interface SelectedCategoryController : UIViewController <UITableViewDataSource,UITableViewDelegate,MBProgressHUDDelegate>

@property(nonatomic,strong) NSArray *CategoryImg;
@property(nonatomic,strong) NSArray *CategorySong;
@property(nonatomic,strong) NSArray *CategoryTime;
@property(nonatomic,strong) NSArray *CategoryArtist;
@property(nonatomic,strong) NSArray *CategoryViews;

@property (weak, nonatomic) IBOutlet UITableView *selectedCategoryTableView;

@end
