//
//  welcome.m
//  slipperySlickProductions
//
//  Created by Mrinal Khullar on 7/13/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "welcomeView.h"


@interface welcomeView ()
{
    NSString *strObj;
    
}
@end

@implementation welcomeView
@synthesize objOfmoviePlayer;


- (void)viewDidLoad
{
     NSLog(@"Welcome View");
     [super viewDidLoad];
   
}

-(void)viewWillAppear:(BOOL)animated
{
    
    self.navigationController.navigationBar.hidden = YES;
  
    strObj = [[NSBundle mainBundle]pathForResource:@"Same Girl - [HQ] [Webmusic.IN]" ofType:@"mp4"];
    objOfmoviePlayer = [[MPMoviePlayerController alloc]initWithContentURL:[NSURL fileURLWithPath:strObj]];
    objOfmoviePlayer.view.frame = _subView.frame;
    
    NSLog(@"%@",NSStringFromCGRect(_subView.frame));
    
    objOfmoviePlayer.scalingMode = MPMovieControlStyleFullscreen;
    [_subView addSubview:objOfmoviePlayer.view];
    [objOfmoviePlayer play];
    

}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    
    
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@",error);
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}


- (IBAction)LoginBtn:(id)sender
{
    NSLog(@"LoginBtn Clicked");
    
    LoginViewController *login = [self.storyboard instantiateViewControllerWithIdentifier:@"go"];
    [self.navigationController pushViewController:login animated:YES];
    [objOfmoviePlayer stop];
}

- (IBAction)SignupBtn:(id)sender
{
    NSLog(@"SignupBtn Clicked ");
    
    signupViewController *registerView = [self.storyboard instantiateViewControllerWithIdentifier:@"move"];
    [self presentViewController:registerView animated:NO completion:nil];
    [objOfmoviePlayer stop];
  
}
@end
