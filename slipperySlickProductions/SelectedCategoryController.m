//
//  SelectedCategoryController.m
//  slipperySlickProductions
//
//  Created by Mrinal Khullar on 7/13/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "SelectedCategoryController.h"

@interface SelectedCategoryController ()

@end

@implementation SelectedCategoryController
@synthesize CategoryImg,CategorySong,CategoryTime,CategoryArtist,CategoryViews,selectedCategoryTableView;

- (void)viewDidLoad
{
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.title = @"SELECTED CATEGORY SONGS";
    
    UIButton *btnNext1 =[[UIButton alloc] init];
    [btnNext1 setBackgroundImage:[UIImage imageNamed:@"left-arrow.png"] forState:UIControlStateNormal];
    
    btnNext1.frame = CGRectMake(5, 5, 20,20);
    UIBarButtonItem *btnNext =[[UIBarButtonItem alloc] initWithCustomView:btnNext1];
    [btnNext1 addTarget:self action:@selector(backBtn:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = btnNext;
    
    
       
    
    [super viewDidLoad];
    
    CategoryImg = [NSArray arrayWithObjects: @"firstImage640*250.png",@"thirdImage640*250.jpg",@"secImage640*250.jpg",@"fourthImage640x250.jpg",@"artist10.jpg",@"artist6.jpeg",@"artist7.jpeg",@"firstImage640*250.png",@"artist11.jpeg",@"artist10.jpg", nil];
    CategoryArtist = [NSArray arrayWithObjects:@"Jordon Fisher",@"Lucy Hale",@"Olivia Holt",@"Sabrina Carpenter",@"Zella Day",@"Zendaya",@"Justin Bieber",@"Miley Cyrus",@"Selena Gomez",@"Haylie Duff", nil];
    CategorySong = [NSArray arrayWithObjects:@"Made with the Flaxen Hair.mp3",@"Footloose:Blue-Ray",@"Footloose:Than & Now",@"Kenny Loggins Talks Footloose",@"Maid with the Flaxen Hair.mp3",@"Inspector Norse",@"Maid with the Flaxen Hair.mp3",@"Inspector Norse",@"Get Busy",@"Jungle Boogie", nil];
    CategoryViews = [NSArray arrayWithObjects:@"7,123",@"1,456",@"2,009",@"1,113",@"4,068",@"41,900",@"34,097",@"12,093",@"22,000",@"55,034", nil];
    CategoryTime = [NSArray arrayWithObjects:@"3:26",@"2:33",@"2:22",@"4:55",@"3:34",@"6:21",@"4:47",@"5:11",@"1:59",@"3:43", nil];
 
    [selectedCategoryTableView reloadData];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    MBProgressHUD *HUD;
    
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.delegate = self;
    HUD.labelText = @"Loading";
    HUD.detailsLabelText = @"updating data";
    HUD.dimBackground = YES;
    [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
}

#pragma mark - Execution code

- (void)myTask
{
    
    sleep(3);
}


-(IBAction)backBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}
#pragma Table View Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return [CategoryImg count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"selectedIdentifier"];
    if (cell == nil)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"selectedIdentifier" forIndexPath:indexPath];
        NSLog(@"cell found");
    }
    
    cell.layoutMargins = UIEdgeInsetsZero;
    cell.preservesSuperviewLayoutMargins = false;
    tableView.separatorInset = UIEdgeInsetsZero;
    
    UILabel *albumTrack = [UILabel new];
    albumTrack.frame = CGRectMake(self.view.frame.size.width-35, 2, 50, 20);
    [albumTrack setTextColor:[UIColor blackColor]];
     albumTrack.font = [UIFont systemFontOfSize:12.0];
    [albumTrack setBackgroundColor:[UIColor whiteColor]];
    [cell.contentView addSubview: albumTrack];
    albumTrack.text = CategoryTime[indexPath.row];
    
    
    
    UIImageView *lblImg = [[UIImageView alloc]init];
    lblImg.frame = CGRectMake(2, 5, 40, 40);
    lblImg.image = [UIImage imageNamed:CategoryImg[indexPath.row]];
    [cell.contentView addSubview:lblImg];
    
    UILabel *albumArtist = [UILabel new];
    albumArtist.frame = CGRectMake(50, 2, 150, 22);
    [albumArtist setTextColor:[UIColor blackColor]];
    albumArtist.font = [UIFont systemFontOfSize:10.0];
    [albumArtist setBackgroundColor:[UIColor whiteColor]];
    [cell.contentView addSubview: albumArtist] ;
    albumArtist.text = CategoryArtist[indexPath.row];
    
    UILabel *albumCat = [UILabel new];
    albumCat.frame = CGRectMake(50, 20, 200, 14);
    [albumCat setTextColor:[UIColor blackColor]];
    albumCat.font = [UIFont boldSystemFontOfSize:10.0];
    [albumCat setBackgroundColor:[UIColor whiteColor]];
    [cell.contentView addSubview: albumCat] ;
    albumCat.text = CategorySong[indexPath.row];

    
    UIImageView *staticImg = [[UIImageView alloc]init];
    staticImg.frame = CGRectMake(lblImg.frame.size.width+10,lblImg.frame.size.height-4, 10, 10);
    staticImg.image = [UIImage imageNamed:@"playStaticImage.png"];
    [cell.contentView addSubview:staticImg];

    UILabel *albumViews = [UILabel new];
    albumViews.frame = CGRectMake(lblImg.frame.size.width+28, 33, 40, 15);
    [albumViews setTextColor:[UIColor blackColor]];
    albumViews.font = [UIFont systemFontOfSize:10.0];
    [albumViews setBackgroundColor:[UIColor whiteColor]];
    [cell.contentView addSubview: albumViews];
    albumViews.text = CategoryViews[indexPath.row];
    
       return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SelectedSongController *selectedSong = [self.storyboard instantiateViewControllerWithIdentifier:@"selectedSong"];
    selectedSong.strImage = CategoryImg[indexPath.row];
    selectedSong.strNavigationTitle = CategorySong[indexPath.row];
    selectedSong.strSongTotalTime = CategoryTime[indexPath.row];
    selectedSong.strArtistName = CategoryArtist[indexPath.row];
    [self.navigationController pushViewController:selectedSong animated:NO];
    
    
}
@end
