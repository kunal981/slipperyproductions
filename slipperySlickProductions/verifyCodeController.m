//
//  verifyCodeController.m
//  slipperySlickProductions
//
//  Created by Mrinal Khullar on 8/10/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "verifyCodeController.h"

@interface verifyCodeController ()

@end

@implementation verifyCodeController
@synthesize strEmail,strToken;


- (void)viewDidLoad
{
    _outletOfVerificationCode.layer.borderWidth = 1.5f;
    _outletOfVerificationCode.layer.borderColor = [[UIColor whiteColor] CGColor];
    

    [super viewDidLoad];
    
    
    _textFiledOfVerificationCode.text = strToken;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)verificationBtn:(id)sender
{
    
    if ([_textFiledOfVerificationCode.text isEqual:strToken])
    {
        NSLog(@"token matched ");
    }
    
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Wrong Verification Code" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
    }

   
    
}
@end
