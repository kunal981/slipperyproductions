//
//  RegistrationViewController.m
//  slipperySlickProductions
//
//  Created by Mrinal Khullar on 7/21/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "signUpViewController.h"

@interface signupViewController ()


@end

@implementation signupViewController
@synthesize outletOfBtnBack;

- (void)viewDidLoad
{
  outletOfBtnBack.layer.borderWidth=1.5f;
    outletOfBtnBack.layer.borderColor=[[UIColor whiteColor] CGColor];
    
    _outletOfSubmitBtn.layer.borderWidth = 1.5f;
    _outletOfSubmitBtn.layer.borderColor = [[UIColor whiteColor] CGColor];
    [super viewDidLoad];
    
    _firstName.delegate = self;
    _lastName.delegate = self;
    _email.delegate = self;
    _userName.delegate = self;
   _password.delegate = self;

    
    // Do any additional setup after loading the view.
}


-(void)signUp
{
    
    NSString *post = [NSString stringWithFormat:@"firstname=%@&lastname=%@&username=%@&email=%@&password=%@",_firstName.text ,_lastName.text,_userName.text,_email.text,_password.text];
    NSLog(@"post is %@ %@ %@ %@ %@ ",_firstName.text,_lastName.text,_email.text,_userName.text,_password.text);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:signUpAPI]];
    
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSURLConnection *theConnection = [NSURLConnection connectionWithRequest:request delegate:self];
    
    if (theConnection)
    {
        NSLog(@"theConnection is succesful");
        
        signUpMutableData = [NSMutableData data];
        
    }
    else
    {
        NSLog(@"theConnection failed");
    }
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    signUpMutableData = [[NSMutableData alloc]init];
    
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    NSLog(@" data is %@",data);
    
    NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
    signUpMutableData = [[NSMutableData alloc]initWithData:data];
    
    NSLog(@"signUpMutableData is %@",signUpMutableData);
    }

// This method receives the error report in case of connection is not made to server.


// This method is used to process the data after connection has made successfully.
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    id result = [NSJSONSerialization JSONObjectWithData:signUpMutableData options:kNilOptions error:nil];
    NSLog(@"result is %@",result);
    
//    NSLog(@"connectionDidFinishLoading");
//    NSLog(@"Succeeded! Received %d bytes of data",[signUpMutableData length]);
//    
//    // convert to JSON
//    NSError *myError = nil;
//    NSDictionary *res = [NSJSONSerialization JSONObjectWithData:signUpMutableData options:NSJSONReadingMutableLeaves error:&myError];
//    
//    // show all values
//    for(id key in res)
//    {
//        
//        id value = [res objectForKey:key];
//        
////        NSString *keyAsString = (NSString *)key;
////        NSString *valueAsString = (NSString *)value;
//        
//        NSLog(@"firstname=%@", _firstName.text);
//        NSLog(@"lastname=%@", _lastName.text);
//        NSLog(@"username=%@",_email.text);
//        NSLog(@"")
//    }
//    
//    // extract specific value...
//    NSArray *results = [res objectForKey:@"results"];
//    
//    for (NSDictionary *result in results)
//    {
//        NSString *icon = [result objectForKey:@"icon"];
//        NSLog(@"icon: %@", icon);
//    }
    
}
    



- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@",error);

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_firstName resignFirstResponder ];
    [_lastName resignFirstResponder ];
    [_email resignFirstResponder ];
    [_userName resignFirstResponder ];
   [_password resignFirstResponder ];
   
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_firstName resignFirstResponder ];
    [_lastName resignFirstResponder ];
    [_email resignFirstResponder ];
    [_userName resignFirstResponder ];
    [_password resignFirstResponder ];
    
    return YES;
    
}

-(BOOL)checkingEmail:(NSString *)checkEmail
{
    NSLog(@"checking Email");
    BOOL var1 = true;
    
    
    if ([checkEmail containsString:@"@"])                                                           // if string contains @
    {
        NSArray *myWords = [checkEmail componentsSeparatedByString:@"@"];        //myWords is an array,separate strings by @ and keeping string into array
        
        for (int i = 0; i<[myWords count]; i++)                                                       //checks string one by one
        {
            if ([myWords[i] isEqual: @""])                                             // if string is empty than return false and myWords[i] is a string
            {
                var1 = false;
            }
        }
        
        if (var1)
        {
            
            if ([myWords[[myWords count] - 1] containsString:@"."])
            {
                NSLog(@"working %@",myWords[[myWords count] - 1]);
                
                NSArray *myDotWords = [myWords[[myWords count] - 1] componentsSeparatedByString:@"."];
                
                for (int i = 0; i<[myDotWords count]; i++)
                {
                    if ([myDotWords[i] isEqual: @""])
                    {
                        var1 = false;
                    }
                }
            }
            else
            {
                var1 = false;
            }
        }
    }
    else
    {
        var1 = false;
    }
    
    
    return var1;
}

- (BOOL)strongPassword:(NSString *)yourText
{
    BOOL strongPwd = YES;
    
    //Checking length
    if([yourText length] < 8)
        strongPwd = NO;
    
    //Checking uppercase characters
  NSCharacterSet  *charSet = [NSCharacterSet uppercaseLetterCharacterSet];
    NSRange range = [yourText rangeOfCharacterFromSet:charSet];
    if(range.location == NSNotFound)
        strongPwd = NO;
    
    //Checking lowercase characters
    charSet = [NSCharacterSet lowercaseLetterCharacterSet];
    range = [yourText rangeOfCharacterFromSet:charSet];
    if(range.location == NSNotFound)
        strongPwd = NO;
    
    //Checking special characters
    charSet = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    range = [yourText rangeOfCharacterFromSet:charSet];
    if(range.location == NSNotFound)
        strongPwd = NO;
    
    return strongPwd;
}



- (IBAction)submitBtn:(id)sender
{
    if([_firstName.text isEqualToString:@""] || [_lastName.text isEqualToString:@""] || [_email.text isEqualToString:@""] ||[_userName.text isEqualToString:@""] ||  [_password.text isEqualToString:@""])
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"All textFields are mandatory" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
        
    }
    
       else  if (![self checkingEmail:_email.text] )
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"InValid Email" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
        
    
       else  if ( ![self strongPassword:_password.text])
       {
           UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"InValid Password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
           [alert show];
       }

    
    
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Valid Email and Password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [self signUp];

    }
   
 }


- (IBAction)backBtn:(id)sender
{
    
    [self dismissViewControllerAnimated:NO completion:nil];
    welcomeView *sign = [welcomeView new];
    [sign.objOfmoviePlayer pause];
   
}


- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField isEqual:_firstName])
    {
        if ([string isEqualToString:@" "] )
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Firstname can't be filled with blank space" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            
            return NO;
            
        }
    }
    if ([textField isEqual:_lastName])
    {
        if ([string isEqualToString:@" "] )
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"lastname can't be filled with blank space" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            
            return NO;
            
        }
    }
    
    if ([textField isEqual:_email])
    {
        if ([string isEqualToString:@" "] )
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Email can't be filled with blank space" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            
            return NO;
            
        }
    }
    if ([textField isEqual:_userName])
    {
        if ([string isEqualToString:@" "] )
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Username can't be filled with blank space" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            
            return NO;
            
        }
    }

    if ([textField isEqual:_password])
    {
        if ([string isEqualToString:@" "] )
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Password can't be filled with blank space" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            
            return NO;
            
        }
    }

    return YES;
}


@end
