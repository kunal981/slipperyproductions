//
//  AlbumViewController.h
//  slipperySlickProductions
//
//  Created by Mrinal Khullar on 7/10/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectedCategoryController.h"
#import <QuartzCore/QuartzCore.h>
#import "MBProgressHUD.h"



@interface AlbumViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,MBProgressHUDDelegate>

@property(nonatomic,strong) NSArray *imgArray;
@property(nonatomic,strong) NSArray *categoryArray;
@property(nonatomic,strong) NSArray *trackArray;
@property (weak, nonatomic) IBOutlet UIView *viewSearchBar;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldOfSearchBar;
@property (weak, nonatomic) IBOutlet UIView *outletOfUploadView;

- (IBAction)uploadBtn:(id)sender;






@end



