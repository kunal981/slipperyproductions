//
//  LibraryViewController.m
//  slipperySlickProductions
//
//  Created by Mrinal Khullar on 7/10/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import "DiscoverViewController.h"
#define UIColorFromRGB(rgbValue)  [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface DiscoverViewController ()

@end

@implementation DiscoverViewController

@synthesize discoverFirst,discoverSecond,discoverThird;
@synthesize genresImgs,genresName,genresArtist,genresViews,genresTime;
@synthesize musicImgs,musicName,musicTime,musicArtist,musicViews;
@synthesize audioImgs,audioName,audioTime,audioArtist,audioViews;

- (void)viewDidLoad
{
   
    [discoverFirst.layer setBorderWidth:1.5f];
    [discoverFirst.layer setBorderColor:[UIColorFromRGB(0X62C9ED) CGColor]];
    [discoverSecond.layer setBorderWidth:1.5f];
    [discoverSecond.layer setBorderColor:[UIColorFromRGB(0X62C9ED) CGColor]];
    [discoverThird.layer setBorderWidth:1.5f];
    [discoverThird.layer setBorderColor:[UIColorFromRGB(0X62C9ED) CGColor]];
    
    _genresTableView.frame = CGRectMake(_genresTableView.frame.origin.x, _discoverViewObj.frame.size.height, _genresTableView.frame.size.width, _genresTableView.frame.size.height);
    _musicTableView.frame = CGRectMake(_musicTableView.frame.origin.x, _discoverViewObj.frame.size.height, _musicTableView.frame.size.width, _musicTableView.frame.size.height);
    _audioTableView.frame = CGRectMake(_audioTableView.frame.origin.x, _discoverViewObj.frame.size.height, _audioTableView.frame.size.width, _audioTableView.frame.size.height);
    
    
    
   
    [self changeButtonBackGroundColor:discoverFirst fontColor:UIColorFromRGB(0xFFFFFF) backgroundColor:UIColorFromRGB(0X62C9ED)];
    [self changeButtonBackGroundColor:discoverSecond fontColor:UIColorFromRGB(0X62C9ED) backgroundColor:UIColorFromRGB(0xFFFFFF)];
    [self changeButtonBackGroundColor:discoverThird fontColor:UIColorFromRGB(0X62C9ED) backgroundColor:UIColorFromRGB(0xFFFFFF)];
    
    [super viewDidLoad];
    
    genresArtist = [NSArray arrayWithObjects:@"Jordon Fisher",@"Lucy Hale",@"Olivia Holt",@"Sabrina Carpenter", nil ];
    genresName = [NSArray arrayWithObjects:@"Made with the Flaxen Hair.mp3",@"Footloose:Blue-Ray",@"Footloose:Than & Now",@"Kenny Loggins Talks Footloose", nil];
    genresImgs = [NSArray arrayWithObjects: @"secImage640*250.jpg",@"thirdImage640*250.jpg",@"fourthImage640x250.jpg",@"thirdImage640*250.jpg",nil];
    genresViews = [NSArray arrayWithObjects:@"4,214",@"1,122",@"3,405",@"2,475", nil];
    genresTime = [NSArray arrayWithObjects:@"4:33",@"2:16",@"3:14",@"2:10", nil];
    
    musicArtist = [NSArray arrayWithObjects:@"Olivia Holt",@"Sabrina Carpenter",@"Lucy Hale",@"Jordon Fisher", nil ];
    musicName = [NSArray arrayWithObjects:@"Kenny Loggins Talks Footloose",@"Made with the Flaxen Hair.mp3",@"Footloose:Blue-Ray",@"Footloose:Than & Now", nil];
    musicImgs = [NSArray arrayWithObjects: @"firstImage640*250.png",@"no2.jpg",@"secImage640*250.jpg",@"secImg.png",nil];
    musicViews = [NSArray arrayWithObjects:@"6,314",@"1,142",@"3,475",@"5,475", nil];
    musicTime = [NSArray arrayWithObjects:@"6:33",@"2:56",@"3:54",@"2:50", nil];
    
    audioArtist = [NSArray arrayWithObjects:@"Sabrina Carpenter",@"Lucy Hale",@"Jordon Fisher",@"Olivia Holt", nil ];
    audioName = [NSArray arrayWithObjects:@"Footloose:Blue-Ray",@"Footloose:Than & Now",@"Kenny Loggins Talks Footloose",@"Made with the Flaxen Hair.mp3", nil];
    audioImgs = [NSArray arrayWithObjects: @"thirdImage640*250.jpg",@"secImage640*250.jpg",@"thirdImage640*250.jpg",@"firstImg.png",nil];
    audioViews = [NSArray arrayWithObjects:@"7,111",@"3,102",@"8,475",@"5,407", nil];
    audioTime = [NSArray arrayWithObjects:@"3:33",@"1:56",@"3:04",@"2:00", nil];

    
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    MBProgressHUD *HUD;
    
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.delegate = self;
    HUD.labelText = @"Loading";
    HUD.detailsLabelText = @"updating data";
    HUD.dimBackground = YES;
    [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
}

#pragma mark - Execution code

- (void)myTask
{
    
    sleep(3);
}


-(void)changeButtonBackGroundColor:(UIButton*)sender fontColor:(UIColor*)fontColor  backgroundColor:(UIColor*)backgroundColor
{
          NSLog(@"change Btn Color");
    
        [sender setBackgroundColor: backgroundColor];
        [sender setTitleColor:fontColor forState:UIControlStateNormal];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == _genresTableView)
    {
        return 1;
    }
    
    else if (tableView == _musicTableView)
    {
        
    return 1;
    }
    else
    {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _genresTableView)
    {
    return [genresImgs count];
    }
    
    else if(tableView == _musicTableView)
    {
        return [musicImgs count];
    }
    else
    {
        return [audioImgs count];
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (tableView == _genresTableView)
    {
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    
    
    cell.layoutMargins = UIEdgeInsetsZero;
    cell.preservesSuperviewLayoutMargins = false;
    _genresTableView.separatorInset = UIEdgeInsetsZero;
    
    
    
    UIImageView *lblImg = [[UIImageView alloc]init];
    lblImg.frame = CGRectMake(5, 0, self.view.frame.size.width-10, 95);
    lblImg.image = [UIImage imageNamed:[genresImgs objectAtIndex:indexPath.row]];
  //  lblImg.contentMode = UIViewContentModeScaleAspectFit;
    [cell.contentView addSubview:lblImg];
    
    UILabel *albumCat = [UILabel new];
    albumCat.frame = CGRectMake(15, 40, 150, 25);
    [albumCat setTextColor:[UIColor whiteColor]];
     albumCat.font = [UIFont systemFontOfSize:12];
    //[albumCat setBackgroundColor:[UIColor whiteColor]];
    [cell.contentView addSubview: albumCat] ;
    albumCat.text = genresArtist[indexPath.row];
    
    
    UILabel *lblTrack = [UILabel new];
    lblTrack.frame = CGRectMake(15, 60, 250, 25);
    [lblTrack setTextColor:[UIColor whiteColor]];
     lblTrack.font = [UIFont systemFontOfSize:14];
    //[lblTrack setBackgroundColor:[UIColor whiteColor]];
    [cell.contentView addSubview: lblTrack];
    lblTrack.text = genresName[indexPath.row];
    
    UIImageView *staticImg = [[UIImageView alloc]init];
    staticImg.frame = CGRectMake(5, 100, 15, 15);
    staticImg.image = [UIImage imageNamed:@"playStaticImage.png"];
    //  lblImg.contentMode = UIViewContentModeScaleAspectFit;
    [cell.contentView addSubview:staticImg];
    
    UILabel *noOfViews = [UILabel new];
    noOfViews.frame = CGRectMake(24, 95, 150, 25);
    [noOfViews setTextColor:[UIColor blackColor]];
     noOfViews.font = [UIFont systemFontOfSize:12];
    //[lblTrack setBackgroundColor:[UIColor whiteColor]];
    [cell.contentView addSubview: noOfViews];
    noOfViews.text = genresViews[indexPath.row];
    
    UILabel *songTime = [UILabel new];
    songTime.frame = CGRectMake(self.view.frame.size.width-30, 95, 150, 25);
    [songTime setTextColor:[UIColor blackColor]];
    songTime.font = [UIFont systemFontOfSize:12];
    //[lblTrack setBackgroundColor:[UIColor whiteColor]];
    [cell.contentView addSubview: songTime];
    songTime.text = genresTime[indexPath.row];


    

    
    return cell;
        
        
    }
    else if (tableView == _musicTableView)
    {
        UITableViewCell *musicCell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"musicCell"];
        
        
        musicCell.layoutMargins = UIEdgeInsetsZero;
        musicCell.preservesSuperviewLayoutMargins = false;
        _musicTableView.separatorInset = UIEdgeInsetsZero;
        
        
        
        UIImageView *lblImg = [[UIImageView alloc]init];
        lblImg.frame = CGRectMake(5, 0, self.view.frame.size.width-10, 95);
        lblImg.image = [UIImage imageNamed:[musicImgs objectAtIndex:indexPath.row]];
        //  lblImg.contentMode = UIViewContentModeScaleAspectFit;
        [musicCell.contentView addSubview:lblImg];
        
        UILabel *albumCat = [UILabel new];
        albumCat.frame = CGRectMake(15, 40, 150, 25);
        [albumCat setTextColor:[UIColor whiteColor]];
        albumCat.font = [UIFont systemFontOfSize:12];
        //[albumCat setBackgroundColor:[UIColor whiteColor]];
        [musicCell.contentView addSubview: albumCat] ;
        albumCat.text = musicArtist[indexPath.row];
        
        
        UILabel *lblTrack = [UILabel new];
        lblTrack.frame = CGRectMake(15, 60, 250, 25);
        [lblTrack setTextColor:[UIColor whiteColor]];
        lblTrack.font = [UIFont systemFontOfSize:14];
        //[lblTrack setBackgroundColor:[UIColor whiteColor]];
        [musicCell.contentView addSubview: lblTrack];
        lblTrack.text = musicName[indexPath.row];
        
        UIImageView *staticImg = [[UIImageView alloc]init];
        staticImg.frame = CGRectMake(5, 100, 15, 15);
        staticImg.image = [UIImage imageNamed:@"playStaticImage.png"];
         // lblImg.contentMode = UIViewContentModeScaleAspectFit;
        [musicCell.contentView addSubview:staticImg];
        
        UILabel *noOfViews = [UILabel new];
        noOfViews.frame = CGRectMake(24, 95, 150, 25);
        [noOfViews setTextColor:[UIColor blackColor]];
        noOfViews.font = [UIFont systemFontOfSize:12];
        //[lblTrack setBackgroundColor:[UIColor whiteColor]];
        [musicCell.contentView addSubview: noOfViews];
        noOfViews.text = musicViews[indexPath.row];
        
        UILabel *songTime = [UILabel new];
        songTime.frame = CGRectMake(self.view.frame.size.width-30, 95, 150, 25);
        [songTime setTextColor:[UIColor blackColor]];
        songTime.font = [UIFont systemFontOfSize:12];
        //[lblTrack setBackgroundColor:[UIColor whiteColor]];
        [musicCell.contentView addSubview: songTime];
        songTime.text = musicTime[indexPath.row];
        
        
              return musicCell;
        

    }
    else
    {
        UITableViewCell *audioCell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"audioCell"];
        
        
        audioCell.layoutMargins = UIEdgeInsetsZero;
        audioCell.preservesSuperviewLayoutMargins = false;
        _audioTableView.separatorInset = UIEdgeInsetsZero;
        
        
        
        UIImageView *lblImg = [[UIImageView alloc]init];
        lblImg.frame = CGRectMake(5, 0, self.view.frame.size.width-10, 95);
        lblImg.image = [UIImage imageNamed:[audioImgs objectAtIndex:indexPath.row]];
        //  lblImg.contentMode = UIViewContentModeScaleAspectFit;
        [audioCell.contentView addSubview:lblImg];
        
        UILabel *albumCat = [UILabel new];
        albumCat.frame = CGRectMake(15, 40, 150, 25);
        [albumCat setTextColor:[UIColor whiteColor]];
        albumCat.font = [UIFont systemFontOfSize:12];
        //[albumCat setBackgroundColor:[UIColor whiteColor]];
        [audioCell.contentView addSubview: albumCat] ;
        albumCat.text = audioArtist[indexPath.row];
        
        
        UILabel *lblTrack = [UILabel new];
        lblTrack.frame = CGRectMake(15, 60, 250, 25);
        [lblTrack setTextColor:[UIColor whiteColor]];
        lblTrack.font = [UIFont systemFontOfSize:14];
        //[lblTrack setBackgroundColor:[UIColor whiteColor]];
        [audioCell.contentView addSubview: lblTrack];
        lblTrack.text = audioName[indexPath.row];
        
        UIImageView *staticImg = [[UIImageView alloc]init];
        staticImg.frame = CGRectMake(5, 100, 15, 15);
        staticImg.image = [UIImage imageNamed:@"playStaticImage.png"];
        //  lblImg.contentMode = UIViewContentModeScaleAspectFit;
        [audioCell.contentView addSubview:staticImg];
        
        UILabel *noOfViews = [UILabel new];
        noOfViews.frame = CGRectMake(24, 95, 150, 25);
        [noOfViews setTextColor:[UIColor blackColor]];
        noOfViews.font = [UIFont systemFontOfSize:12];
        //[lblTrack setBackgroundColor:[UIColor whiteColor]];
        [audioCell.contentView addSubview: noOfViews];
        noOfViews.text = audioViews[indexPath.row];
        
        UILabel *songTime = [UILabel new];
        songTime.frame = CGRectMake(self.view.frame.size.width-30, 95, 150, 25);
        [songTime setTextColor:[UIColor blackColor]];
        songTime.font = [UIFont systemFontOfSize:12];
        //[lblTrack setBackgroundColor:[UIColor whiteColor]];
        [audioCell.contentView addSubview: songTime];
        songTime.text = audioTime[indexPath.row];
        
        
        return audioCell;

    }

 
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     SelectedSongController *select = [self.storyboard instantiateViewControllerWithIdentifier:@"selectedSong"];
    
    if(tableView == _genresTableView)
    {
   
    select.strImage = genresImgs[indexPath.row];
    select.strNavigationTitle = genresName[indexPath.row];
    select.strSongTotalTime = genresTime[indexPath.row];
    select.strArtistName = genresArtist[indexPath.row];
    }
    else if (tableView == _musicTableView)
    {
    
    select.strImage = musicImgs[indexPath.row];
    select.strNavigationTitle = musicName[indexPath.row];
    select.strSongTotalTime = musicTime[indexPath.row];
    select.strArtistName = musicArtist[indexPath.row];
    }
    else
    {
    select.strImage = audioImgs[indexPath.row];
    select.strNavigationTitle = audioName[indexPath.row];
    select.strSongTotalTime = audioTime[indexPath.row];
    select.strArtistName = audioArtist[indexPath.row];
    }

    [self.navigationController pushViewController:select animated:NO];
    
    
}
- (IBAction)genresBtn:(id)sender
{
    _genresTableView.hidden = NO;
    _musicTableView.hidden = YES;
    _audioTableView.hidden = YES;
    [self changeButtonBackGroundColor:sender fontColor:UIColorFromRGB(0xFFFFFF) backgroundColor:UIColorFromRGB(0X62C9ED)];
    [self changeButtonBackGroundColor:discoverSecond fontColor:UIColorFromRGB(0X62C9ED) backgroundColor:UIColorFromRGB(0xFFFFFF)];
    [self changeButtonBackGroundColor:discoverThird fontColor:UIColorFromRGB(0X62C9ED) backgroundColor:UIColorFromRGB(0xFFFFFF)];
}

- (IBAction)musicBtn:(id)sender
{
    _genresTableView.hidden = YES;
    _musicTableView.hidden = NO;
    _audioTableView.hidden = YES;
    [self changeButtonBackGroundColor:sender fontColor:UIColorFromRGB(0xFFFFFF) backgroundColor:UIColorFromRGB(0X62C9ED)];
    [self changeButtonBackGroundColor:discoverFirst fontColor:UIColorFromRGB(0X62C9ED) backgroundColor:UIColorFromRGB(0xFFFFFF)];
    [self changeButtonBackGroundColor:discoverThird fontColor:UIColorFromRGB(0X62C9ED) backgroundColor:UIColorFromRGB(0xFFFFFF)];
}

- (IBAction)audioBtn:(id)sender
{
    _genresTableView.hidden = YES;
    _musicTableView.hidden = YES;
    _audioTableView.hidden = NO;
    [self changeButtonBackGroundColor:sender fontColor:UIColorFromRGB(0xFFFFFF) backgroundColor:UIColorFromRGB(0X62C9ED)];
    [self changeButtonBackGroundColor:discoverSecond fontColor:UIColorFromRGB(0X62C9ED) backgroundColor:UIColorFromRGB(0xFFFFFF)];
    [self changeButtonBackGroundColor:discoverFirst fontColor:UIColorFromRGB(0X62C9ED) backgroundColor:UIColorFromRGB(0xFFFFFF)];
}
@end
