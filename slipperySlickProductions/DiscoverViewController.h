//
//  LibraryViewController.h
//  slipperySlickProductions
//
//  Created by Mrinal Khullar on 7/10/15.
//  Copyright (c) 2015 Mrinal Khullar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "SelectedSongController.h"
#import "MBProgressHUD.h"

@interface DiscoverViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,MBProgressHUDDelegate>

- (IBAction)genresBtn:(id)sender;
- (IBAction)musicBtn:(id)sender;
- (IBAction)audioBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *discoverFirst;
@property (weak, nonatomic) IBOutlet UIButton *discoverSecond;
@property (weak, nonatomic) IBOutlet UIButton *discoverThird;

@property (weak, nonatomic) IBOutlet UITableView *genresTableView;
@property (weak, nonatomic) IBOutlet UITableView *musicTableView;
@property (weak, nonatomic) IBOutlet UITableView *audioTableView;

@property(nonatomic,strong) NSArray *genresImgs;
@property(nonatomic,strong) NSArray *genresArtist;
@property(nonatomic,strong) NSArray *genresName;
@property(nonatomic,strong) NSArray *genresViews;
@property(nonatomic,strong) NSArray *genresTime;

@property(nonatomic,strong) NSArray *musicImgs;
@property(nonatomic,strong) NSArray *musicArtist;
@property(nonatomic,strong) NSArray *musicName;
@property(nonatomic,strong) NSArray *musicViews;
@property(nonatomic,strong) NSArray *musicTime;

@property(nonatomic,strong) NSArray *audioImgs;
@property(nonatomic,strong) NSArray *audioArtist;
@property(nonatomic,strong) NSArray *audioName;
@property(nonatomic,strong) NSArray *audioViews;
@property(nonatomic,strong) NSArray *audioTime;

@property (weak, nonatomic) IBOutlet UIView *discoverViewObj;

@end
